
# SCCA (SAP CX CLI Acorel)
This tool utilizes the SAP Commerce cloud APIs for building and deploying to the SAP Commerce Cloud.

## Installation   

1. Clone this repo
2. `virtualenv scca`
3. `cd scca`
4. `pip install -e requirements.txt`
5. Use the program


## Usage
You can run the following command to see what the availble options are
`./scca -h`

`scca config set --api-token=<token> --sap-subscription-id=<subscriptionId>`   
`scca config show`
`scca build create  --branch=<branch> --name=<name>`   
`scca build progress --build-code=<build-code>`   
`scca build deploy --build-code=<build-code> --environment-code=<environment-code> [--database-update-mode=<update-mode>] [--strategy=<strategy>] [--no-wait]`    
`scca deploy progress --build-code=<build-code>`   

## Options:   
  `-h --help     Show this screen.`      
  `--version     Show version.`    
  `--strategy=<strategy>    The way the deployment is handled [default: ROLLING_UPDATE]`    
  `--database-update-mode=<update-mode>      DB update mode [default: UPDATE]`
  
# Docker 
We also provide a docker file in these repository but also a pre-built image ready to use by any pipeline supporting docker.

## Build the image
`docker build . -t acorel/sap-cx-cli`

## Run the image
`docker run -ti acorel/sap-cx-cli bash`

